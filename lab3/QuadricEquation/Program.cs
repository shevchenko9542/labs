﻿using System;

namespace QuadricEquation
{
    public class CQuadraticEquation
    {
        static void Main(){}

        public static double[] GetSolution(double a, double b, double c)
        {
            double epsilon = 1e-5;
            double D = b * b - 4 * a * c;

            if (a < epsilon && a > -epsilon)
            {
                return null;
            }
            else
            {
                if (D < -epsilon)
                {
                    return null;
                }
                else
                {
                    double[] roots = { (-b - Math.Sqrt(D)) / (2 * a), (-b + Math.Sqrt(D)) / (2 * a) };
                    return roots;
                }
            }
        }    
    }
}
