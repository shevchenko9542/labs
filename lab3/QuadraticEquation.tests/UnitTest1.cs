using QuadricEquation;
using Xunit;


namespace QuadraticEquation.tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {

            double[] solution = CQuadraticEquation.GetSolution(1, -2, 1);
            double[] expected = new double[] { 1, 1 };

            Assert.Equal(expected, solution);

        }

        [Fact]
        public void Test2()
        {

            double[] solution = CQuadraticEquation.GetSolution(1, 0, -1);
            double[] expected = new double[] { -1, 1 };

            Assert.Equal(expected, solution);

        }

        [Fact]
        public void Test3()
        {

            double[] solution = CQuadraticEquation.GetSolution(1, 0, 1);
            double[] expected = null;

            Assert.Equal(expected, solution);

        }
        
        [Fact]
        public void Test4()
        {

            double[] solution = CQuadraticEquation.GetSolution(0, 0, 1);
            double[] expected = null;

            Assert.Equal(expected, solution);

        }

    }
}


