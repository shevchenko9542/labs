using System;
using ESheet;
using System.Collections.Generic;
using Xunit;
using System.Linq;

namespace ESheetTests
{
    public class UnitTest1
    {

        List<Student> students = new List<Student>
        {
                 new Student("Ivan","Dolmatov","Ivanovich",1,new List<Packet>
                 {
                     new Packet("Math",5),
                     new Packet("CS",5),
                     new Packet("TSSA",4.99)
                 }),
                 new Student("Vladimir","Borodin","Maximovich",1,new List<Packet>
                 {
                     new Packet("Math",5),
                     new Packet("CS",4),
                     new Packet("TSSA",5)
                 }),
                 new Student("Maxim","Rikov","Petrovich",1,new List<Packet>
                 {
                     new Packet("Math",3.5),
                     new Packet("CS",5),
                     new Packet("TSSA",3.6)
                 }),
                 new Student("Georgiy","Pobedinsky","Matveevich",1,new List<Packet>
                 {
                     new Packet("Math",4.55),
                     new Packet("CS",3.56),
                     new Packet("TSSA",4.58)
                 }),
                 new Student("Matvey","Boyko","Georgievich",2,new List<Packet>
                 {
                     new Packet("Math",5),
                     new Packet("CS",5),
                     new Packet("TSSA",5)
                 }),
                 new Student("Alexander","Kebab","Petrovich",2,new List<Packet>
                 {
                     new Packet("Math",4.26),
                     new Packet("CS",4.4),
                     new Packet("TSSA",5)
                 }),
                 new Student("Ilya","Lyulya","Kebabovich",2,new List<Packet>
                 {
                     new Packet("Math",2),
                     new Packet("CS",3.4),
                     new Packet("TSSA",4.23)
                 }),
                 new Student("Pavel","Popov","Vadimovich",2,new List<Packet>
                 {
                     new Packet("Math",4),
                     new Packet("CS",4.55),
                     new Packet("TSSA",4.6)
                 }),
                 new Student("Mikhail","Loenko","Alexandrovich",3,new List<Packet>
                 {
                     new Packet("Math",5),
                     new Packet("CS",4.5),
                     new Packet("TSSA",4.5)
                 }),
                 new Student("Pyotr","Izmailov","Pavlovich",3,new List<Packet>
                 {
                     new Packet("Math",3.55),
                     new Packet("CS",3.65),
                     new Packet("TSSA",4)
                 }),
                 new Student("Konstantin","Petrushin","Dmitrievich",3,new List<Packet>
                 {
                     new Packet("Math",4.55),
                     new Packet("CS",4.5),
                     new Packet("TSSA",4.5)
                 }),
                 new Student("Mikhail","Zubenko","Petrovich",3,new List<Packet>
                 {
                     new Packet("Math",4),
                     new Packet("CS",4),
                     new Packet("TSSA",4)
                 })
        };





        [Fact]
        public void MaxAvgMarkStudentTest()
        {
            List<Student> actual = new ExamSheet(students).MaxAvgMarkStudent();
            List<Student> expected = new List<Student>
            {
                new Student("Matvey", "Boyko", "Georgievich", 2, new List<Packet>
                 {
                     new Packet("Math",5),
                     new Packet("CS",5),
                     new Packet("TSSA",5)
                 })
            };
            Assert.True(actual != null);
            Assert.Equal(expected.Count(), actual.Count());
            Assert.True(actual.All(student => expected.Contains(student, new StudentComparer())));
            
        }



        [Fact]
        public void SubjectAvgMarkTest()
        {
            double expected_avgMath = (4 + 4.55 + 3.55 + 5 + 4 + 2 + 4.26 + 5 + 4.55 + 3.5 + 5 + 5) / 12;
            double expected_avgCS = (5 + 4 + 5 + 3.56 + 5 + 4.4 + 3.4 + 4.55 + 4.5 + 3.65 + 4.5 + 4) / 12;
            double expected_avgTSSA = (4.99 + 5 + 3.6 + 4.58 + 5 + 5 + 4.23 + 4.6 + 4.5 + 4 + 4.5 + 4) / 12;
            List<Packet> actual = new ExamSheet(students).SubjectAvgMark();
            List<Packet> expected = new List<Packet>
            {
                new Packet("Math",expected_avgMath),
                new Packet("CS",expected_avgCS),
                new Packet("TSSA",expected_avgTSSA)
            };
            Assert.Equal(expected.Count(), actual.Count());
            Assert.True(actual != null);
            Assert.True(actual.All(packet => expected.Contains(packet, new PacketComparer())));
        }



        [Fact]
        public void SubjectMaxAvgMarkGroupTest()
        {
            List<Packet> actual = new ExamSheet(students).SubjectMaxAvgMarkGroup();
            List<Packet> expected = new List<Packet> 
            { 
                new Packet("Math", 1),
                new Packet("CS", 1),
                new Packet("TSSA", 2)
            };

            Assert.Equal(expected.Count(), actual.Count());
            Assert.True(actual != null);
            Assert.True(actual.All(packet => expected.Contains(packet, new PacketComparer())));
        }
    }

}
