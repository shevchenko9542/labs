﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESheet
{
    public class Student
    {
        public string name;
        public string surname;
        public string patronymic;
        public int group;
        public List<Packet> exams;

        public Student
            (
            string name_constructor,
            string surname_constructor,
            string patronymic_constructor,
            int group_constructor = 0,
            List<Packet> exams_constructor = null
            )
        {
            name = name_constructor;
            surname = surname_constructor;
            patronymic = patronymic_constructor;
            group = group_constructor;
            exams = exams_constructor;
        }
    }



    public class Packet
    {
        public string line;
        public double number;

        public Packet
            (
            string line_constructor = null, 
            double number_constructor = 0
            )
        {
            line = line_constructor;
            number = number_constructor;
        }
    }




    public class StudentComparer : IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            if (Object.ReferenceEquals(x, y)) return true;
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) return false;
            
            PacketComparer packet_comparer = new PacketComparer();

            return
                x.name == y.name &&
                x.surname == y.surname &&
                x.patronymic == y.patronymic &&
                x.group == y.group &&
                x.exams.Count() == y.exams.Count() &&
                x.exams.All(packet => y.exams.Contains(packet, packet_comparer));
        }

        public int GetHashCode(Student student)
        {
            if (Object.ReferenceEquals(student, null)) return 0;

            int hashStudent_name = student.name == null ? 0 : student.name.GetHashCode();
            int hashStudent_surname = student.surname == null ? 0 : student.surname.GetHashCode();
            int hashStudent_patronymic = student.patronymic == null ? 0 : student.patronymic.GetHashCode();
            int hashStudent_group = student.group.GetHashCode();
            int hashStudent_exams = student.exams == null ? 0 : student.exams.GetHashCode();

            return
                hashStudent_name ^
                hashStudent_surname ^
                hashStudent_patronymic ^
                hashStudent_group ^
                hashStudent_exams;
        }

    }




    public class PacketComparer : IEqualityComparer<Packet>
    {
        public bool Equals(Packet x, Packet y)
        {
            if (Object.ReferenceEquals(x, y)) return true;
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) return false;

            return x.line == y.line && x.number == y.number;
        }


        public int GetHashCode(Packet packet)
        {
            if (Object.ReferenceEquals(packet, null)) return 0;

            int hashPacket_line = packet.line == null ? 0 : packet.line.GetHashCode();
            int hashPacket_number = packet.number.GetHashCode();

            return hashPacket_line ^ hashPacket_number;
        }
    }





    public class ExamSheet
    {
        public List<Student> sheet;

        public ExamSheet(List<Student> students = null)
        {
            sheet = students;
        }

        public List<Student> MaxAvgMarkStudent()
        {
            var stdlist = sheet.Select(a => new { student = a, avg = a.exams.Average(u => u.number) });
            double maxavg = stdlist.Max(a => a.avg);
            return stdlist.Where(a => a.avg == maxavg)
                          .Select(a => a.student)
                          .ToList();
        }

        public List<Packet> SubjectAvgMark()
        {
            var tmplist = sheet.Select(a => a.exams)
                               .Aggregate((x, y) => x.Concat(y).ToList());
            return tmplist.Select(a => a.line)
                          .Distinct()
                          .Select(x => new Packet(x, tmplist.Where(a => a.line == x)
                                                              .Average(a => a.number)))
                          .ToList();
        }

        public List<Packet> SubjectMaxAvgMarkGroup()
        {
            var groupedavgmarks = sheet.Select(x => x.group)
                                       .Distinct()
                                       .Select(x => new 
            {
                group = x, 
                marks = sheet.Select(a => a.exams)
                             .Aggregate((u, v) => u.Concat(v).ToList())
                             .Select(a => a.line)
                             .Distinct()
                             .Select(t => new Packet(t, sheet.Where(a => a.group == x)
                                                             .Select(a => a.exams)
                                                             .Aggregate((u, v) => u.Concat(v).ToList())
                                                             .Where(a => a.line == t)
                                                             .Average(a => a.number)))
            });

            return sheet.Select(a => a.exams)
                        .Aggregate((x, y) => x.Concat(y).ToList())
                        .Select(a => a.line)
                        .Distinct()
                        .Select(x => new Packet(x, groupedavgmarks.Select(a => new 
                                                                         {
                                                                             group = a.group, 
                                                                             avgmark = a.marks.Where(u => u.line == x)
                                                                                           .Select(u => u.number)
                                                                                           .Aggregate((x1,x2) => x1 + x2) / a.marks.Count()
                                                                         })
                                                                  .OrderByDescending(a => a.avgmark)
                                                                  .First().group))
                        .ToList();

        }
    }



    class Program
    {
        static void Main(string[] args) { }
    }
}
